# 
# Translators:
# Kisbenedek Márton <kmarton815@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-21 17:24+0000\n"
"Last-Translator: Kisbenedek Márton <kmarton815@gmail.com>\n"
"Language-Team: Hungarian (Hungary) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/hu_HU/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu_HU\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: helloguest.html14, 33
msgid "Welcome"
msgstr "Üdvözöljük"

#: helloguest.html:23
msgid "Entering the Guest Session."
msgstr "Belépés a Vendég munkamenetbe."

#: helloguest.html:24
msgid ""
"Guest Sessions are great for allowing other people to use this computer "
"without tampering with applications, your settings or files."
msgstr "A vendég munkamenet lehetőséget nyújt a számítógép használatára anélkül, hogy alkalmazásokkal, beállításokkal, vagy fájlokkal kellene babrálnunk."

#: helloguest.html:28
msgid "Any data left in the session will be lost when you log out!"
msgstr "Minden a munkamenetben hagyott adat elvész kijelentkezéskor! "

#: helloguest.html:29
msgid ""
"Please transfer anything you'd like to keep to an external drive, a network "
"share or cloud service."
msgstr "Kérlek másolj át mindent egy külső lemezre, hálózati vagy felhő tárhelyre, amit meg szeretnél tartani."

#: helloguest.html:32
msgid "The"
msgstr "Az"

#: helloguest.html:33
msgid ""
"application introduces you to the operating system, but some features that "
"require privileges will be disabled."
msgstr "alkalmazás bevezet az operációs rendszerbe, de néhány funkció, ami jogosultságokat igényel, le lesz tiltva."

#: helloguest.html:36
msgid "Thank you for using Ubuntu MATE."
msgstr "Köszönjük, hogy az Ubuntu MATE-t használod."

#: helloguest.html:38
msgid "Discover"
msgstr "Felfedezés"

#: helloguest.html:39
msgid "Dismiss"
msgstr "Elhagy"
